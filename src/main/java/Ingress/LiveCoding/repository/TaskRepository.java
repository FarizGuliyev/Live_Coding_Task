package Ingress.LiveCoding.repository;

import Ingress.LiveCoding.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task,Long> {
}

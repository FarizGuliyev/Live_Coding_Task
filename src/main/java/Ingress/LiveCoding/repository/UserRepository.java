package Ingress.LiveCoding.repository;

import Ingress.LiveCoding.entity.Task;
import Ingress.LiveCoding.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Long> {
    List<User> findAllByTaskId(Long taskId);

}

package Ingress.LiveCoding.repository;

import Ingress.LiveCoding.entity.Organization;
import Ingress.LiveCoding.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrganizationRepository extends JpaRepository<Organization,Long> {


}

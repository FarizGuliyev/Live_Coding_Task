package Ingress.LiveCoding.controller;

import Ingress.LiveCoding.dto.TaskRequest;
import Ingress.LiveCoding.dto.TaskResponse;
import Ingress.LiveCoding.dto.UserRequest;
import Ingress.LiveCoding.dto.UserResponse;
import Ingress.LiveCoding.service.TaskService;
import Ingress.LiveCoding.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @PostMapping
    public TaskResponse create(@RequestBody @Valid TaskRequest request) {
        return taskService.create(request);
    }

    @GetMapping("/{taskId}")
    public TaskResponse get(@PathVariable Long taskId) {
        return taskService.get(taskId);
    }

    @GetMapping
    public List<TaskResponse> getAll() {
        return taskService.getAll();
    }

    @PutMapping("/{taskId}")
    public TaskResponse update(@PathVariable Long taskId, @RequestBody @Valid TaskRequest taskRequest) {
        return taskService.update(taskId, taskRequest);
    }

    @PutMapping("/{taskId}/withUser/{userId}")
    public TaskResponse updateTaskWithUser(@PathVariable Long taskId, @PathVariable Long userId) {
        return taskService.updateTaskWithUser(userId, taskId);
    }

    @DeleteMapping("/{taskId}")
    public void delete(@PathVariable Long taskId) {
        taskService.delete(taskId);
    }
}

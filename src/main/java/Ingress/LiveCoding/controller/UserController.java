package Ingress.LiveCoding.controller;

import Ingress.LiveCoding.dto.UserRequest;
import Ingress.LiveCoding.dto.UserResponse;
import Ingress.LiveCoding.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping
    public UserResponse create(@RequestBody @Valid UserRequest request) {
        return userService.create(request);
    }

    @GetMapping("/{userId}")
    public UserResponse get(@PathVariable Long userId) {
        return userService.get(userId);
    }

    @GetMapping
    public List<UserResponse> getAll() {
        return userService.getAll();
    }

    @PutMapping("/{userId}")
    public UserResponse update(@PathVariable Long userId, @RequestBody @Valid UserRequest userRequest) {
        return userService.update(userId, userId);
    }

    @PutMapping("/{userId}/withTask/{taskId}")
    public UserResponse updateUserWithTask(@PathVariable Long userId, @PathVariable Long taskId) {
        return userService.updateUserWithTask(userId, taskId);
    }

    @DeleteMapping("/{userId}")
    public void delete(@PathVariable Long userId) {
        userService.delete(userId);
    }
}

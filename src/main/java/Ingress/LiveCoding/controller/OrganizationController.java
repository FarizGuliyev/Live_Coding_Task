package Ingress.LiveCoding.controller;

import Ingress.LiveCoding.dto.OrganizationRequest;
import Ingress.LiveCoding.dto.OrganizationResponse;
import Ingress.LiveCoding.dto.TaskRequest;
import Ingress.LiveCoding.dto.TaskResponse;
import Ingress.LiveCoding.service.OrganizationService;
import Ingress.LiveCoding.service.TaskService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/organization")
@RequiredArgsConstructor
public class OrganizationController {
    private final OrganizationService organizationService;

    @PostMapping
    public OrganizationResponse create(@RequestBody @Valid OrganizationRequest request) {
        return organizationService.create(request);
    }

    @GetMapping("/{organizationId}")
    public OrganizationResponse get(@PathVariable Long organizationId) {
        return organizationService.get(organizationId);
    }

    @GetMapping
    public List<OrganizationResponse> getAll() {
        return organizationService.getAll();
    }

    @PutMapping("/{organizationId}")
    public OrganizationResponse update(@PathVariable Long organizationId, @RequestBody @Valid OrganizationRequest request) {
        return organizationService.update(organizationId, request);
    }

    @DeleteMapping("/{organizationId}")
    public void delete(@PathVariable Long organizationId) {
        organizationService.delete(organizationId);
    }
}

package Ingress.LiveCoding.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequest {
    @NotEmpty
    String name;
    @NotEmpty
    String surname;
    String email;
    @NotEmpty
    String password;
    @NotEmpty
    String validatePassword;
}

package Ingress.LiveCoding.dto;

import Ingress.LiveCoding.entity.User;
import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationResponse {
    Long id;

    @NotEmpty
    String name;

    @Builder.Default
    Set<User> users = new HashSet<>();
}

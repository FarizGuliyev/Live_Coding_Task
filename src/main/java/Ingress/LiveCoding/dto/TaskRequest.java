package Ingress.LiveCoding.dto;

import Ingress.LiveCoding.entity.Status;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskRequest {
    @NotEmpty
    String name;
    @NotEmpty
    Enum<Status> status;
    @NotEmpty
    @JsonFormat(pattern = "dd.MM.yyyy")
    LocalDate deadline;
    String description;
}

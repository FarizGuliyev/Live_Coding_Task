package Ingress.LiveCoding.dto;

import Ingress.LiveCoding.entity.Organization;
import Ingress.LiveCoding.entity.Task;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserResponse {
    Long id;
    @NotEmpty
    String name;
    @NotEmpty
    String surname;
    String email;
    @NotEmpty
    String password;

    Organization organization;

    Set<Task> tasks = new HashSet<>();
}

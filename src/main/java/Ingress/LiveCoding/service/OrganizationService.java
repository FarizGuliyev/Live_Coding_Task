package Ingress.LiveCoding.service;

import Ingress.LiveCoding.dto.OrganizationRequest;
import Ingress.LiveCoding.dto.OrganizationResponse;
import Ingress.LiveCoding.dto.TaskResponse;
import Ingress.LiveCoding.entity.Organization;
import Ingress.LiveCoding.entity.Task;
import Ingress.LiveCoding.entity.User;
import Ingress.LiveCoding.repository.OrganizationRepository;
import Ingress.LiveCoding.repository.TaskRepository;
import Ingress.LiveCoding.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;
    private final ModelMapper modelMapper;

    public OrganizationResponse create(OrganizationRequest request) {
        Organization organization = modelMapper.map(request, Organization.class);
        organization = organizationRepository.save(organization);
        return modelMapper.map(organization, OrganizationResponse.class);
    }


    public OrganizationResponse get(Long organizationId) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() ->
                new RuntimeException(String.format("Organization with %s not found ", organizationId)));
        return modelMapper.map(organization, OrganizationResponse.class);
    }


    public List<OrganizationResponse> getAll() {
        List<Organization> organizations = organizationRepository.findAll();
        List<OrganizationResponse> organizationResponses = new ArrayList<>();
        organizations.forEach(region ->
                organizationResponses.add(modelMapper.map(region, OrganizationResponse.class)));
        return organizationResponses;
    }

    public OrganizationResponse update(Long organizationId, OrganizationRequest request) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() ->
                new RuntimeException(String.format("Organization with %s not found ", organizationId)));

        Organization organization1 = modelMapper.map(request, Organization.class);
        organization1.setId(organization.getId());
        Organization saveOrganization = organizationRepository.save(organization1);
        return modelMapper.map(saveOrganization, OrganizationResponse.class);
    }

    public void delete(Long organizationId) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() ->
                new RuntimeException(String.format("Organization with %s not found ", organizationId)));
        
    }
}

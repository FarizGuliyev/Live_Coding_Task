package Ingress.LiveCoding.service;

import Ingress.LiveCoding.dto.TaskRequest;
import Ingress.LiveCoding.dto.TaskResponse;
import Ingress.LiveCoding.entity.Task;
import Ingress.LiveCoding.entity.User;
import Ingress.LiveCoding.repository.TaskRepository;
import Ingress.LiveCoding.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;


    public TaskResponse create(TaskRequest request) {
        Task task = modelMapper.map(request, Task.class);
        task = taskRepository.save(task);
        return modelMapper.map(task, TaskResponse.class);
    }

    @Transactional
    public TaskResponse get(Long regionId) {
        Task task = taskRepository.findById(regionId).get();
        return modelMapper.map(task, TaskResponse.class);
    }

    public List<TaskResponse> getAll() {
        List<Task> tasks = taskRepository.findAll();
        List<TaskResponse> taskResponses = new ArrayList<>();
        tasks.forEach(region ->
                taskResponses.add(modelMapper.map(region, TaskResponse.class)));
        return taskResponses;
    }

    public TaskResponse update(Long regionId, TaskRequest regionRequest) {
        Task task = taskRepository.findById(regionId).orElseThrow(RuntimeException::new);

        Task task1 = modelMapper.map(regionRequest, Task.class);
        task1.setId(task.getId());
        Task saveTask = taskRepository.save(task1);
        return modelMapper.map(saveTask, TaskResponse.class);
    }

    public void delete(Long regionId) {
        List<User> markets = userRepository.findAllByTaskId(regionId);

        Task task = taskRepository.findById(regionId).orElseThrow(RuntimeException::new);
        markets.forEach(
                market -> market.getTasks().remove(task));

        userRepository.saveAll(markets);

        taskRepository.delete(task);

    }

    public TaskResponse updateTaskWithUser(Long userId, Long taskId) {
        return null;
    }
}

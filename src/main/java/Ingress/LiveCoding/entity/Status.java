package Ingress.LiveCoding.entity;

public enum Status {
    ACTIVE("Task is active, but not running"),
    TODO("Task is running"),
    DONE("Task has done");

    private final String message;

    // Parameters to the enum constructors are the ones in the enum "definition"
    Status(String message)
    {
        this.message = message;
    }

    @Override
    public String toString()
    {
        return message;
    }
    }


